[global/wm]
margin-bottom = 0
margin-top = 0

[colors]
background = #1d2930
foreground = #e8dcca
red = #bc4049
black = #828488
shade1 = #6F2744
shade2 = #972c2c
shade3 = #ab3030
shade4 = #842828
shade5 = #65788F
shade6 = #5E3060
shade7 = #6F2744
shade8 = #353F58
shade9 = #3C4662

yellow = #cda217
blue = #6c9e9e
magenta = #a14f6a
cyan = #76af98
white = #dcd8c0
gray = #c1c1c1
dblue1 = #414f70
dblue2 = #424f6f

;primary = #ffb52a
;secondary = #e60053
;alert = #bd2c40

[bar/top]
monitor = DP-0
width = 100%
;width = 640
;offset-x = 640
;offset-y = 20
height = 22
bottom = false
line-size = 0

dpi-x = 96
dpi-y = 96

fixed-center = true

border-size = 0
border-color = #1d1f21

background = ${colors.background}
foreground = ${colors.foreground}

padding-left = 0
padding-right = 0

module-margin-left = 0
module-margin-right = 0

#font-0 = DejaVu Sans:size=9;2
font-0 = cherry:Regular:size=11;2
font-1 = Iosevka Nerd Font:size=13;3
font-2 = Kochi Gothic:size=10;2

modules-left = powermenu left1 windowtitle left2
modules-center = ewmh
modules-right = right5 mpd right4 hostname right2 alsa right3 date

;separator = ::

cursor-click = pointer
cursor-scroll = ns-resize

tray-position = right
tray-offset-y = -5%
override-redirect = true
tray-maxsize = 18
tray-scale = 1.0
tray-spacing = 1
tray-background = ${colors.shade3}

; left = , , , , , 
; right = , , , , , 

[module/left1]
type = custom/text
content-background = ${colors.shade9}
content-foreground = ${colors.shade8}
content = "%{T3}%{T-}"

[module/left2]
type = custom/text
content-background = ${colors.background}
content-foreground = ${colors.shade9}
content = "%{T3}%{T-}"

[module/left3]
type = custom/text
content-background = ${colors.background}
content-foreground = ${colors.shade9}
content = "%{T3}%{T-}"

[module/left4]
type = custom/text
content-background = ${colors.background}
content-foreground = ${colors.shade1}
content = "%{T3}%{T-}"

[module/right1]
type = custom/text
content-background = ${colors.background}
content-foreground = ${colors.shade1}
content = "%{T3}%{T-}"

[module/right2]
type = custom/text
content-background = ${colors.shade4}
content-foreground = ${colors.shade2}
content = "%{T3}%{T-}"

[module/right3]
type = custom/text
content-background = ${colors.shade2}
content-foreground = ${colors.shade3}
content = "%{T3}%{T-}"

[module/right4]
type = custom/text
content-background = ${colors.shade7}
content-foreground = ${colors.shade4}
content = "%{T3}%{T-}"

[module/right5]
type = custom/text
content-background = ${colors.background}
content-foreground = ${colors.shade7}
content = "%{T3}%{T-}"

[module/sep]
type = custom/text
content =" ﰲ "
content-foreground = ${colors.black}
content-background = ${colors.background}

[module/sep2]
type = custom/text
content = by
content-foreground = ${colors.cyan}
content-background = ${colors.background}

[module/sep3]
type = custom/text
content = |
content-foreground = ${colors.black}
content-background = ${colors.background}

[module/mpd]
type = internal/mpd
label-song = "%title% "
format-online-prefix = " "
format-online-padding = 0
label-song-background = ${colors.shade7}
format-online-prefix-foreground = ${colors.gray}
format-online-prefix-background = ${colors.shade7}
label-song-foreground = ${colors.yellow}
label-offline = mpd is offline

[module/ewmh]
type = internal/xworkspaces
enable-scroll = true
format-padding = 0
format-foreground = ${colors.foreground}
format-background = ${colors.background}

; icon-[0-9]+ = <desktop-name>;<icon>
; NOTE: The desktop name needs to match the name configured by the WM
; You can get a list of the defined desktops using:
; $ xprop -root _NET_DESKTOP_NAMES
#icon-1 = "term;  "
#icon-0 = "web;  "
#icon-2 = "img;  " 
#icon-3 = "irc;  "
#icon-4 = "emu; ﳟ " 

icon-1 = "term;  "
icon-0 = "web;  "
icon-2 = "img;  " 
icon-3 = "irc;  "
icon-4 = "emu;  " 

format = <label-state>
label-monitor = %name%

label-active = %icon%
label-active-foreground = ${colors.shade2}
label-active-background = ${colors.background}
label-active-underline = ${colors.shade2}

label-occupied = %icon%
label-occupied-foreground = ${colors.cyan}

label-urgent = %icon%
label-urgent-background = #bd2c40
label-urgent-underline = #9b0a20

label-empty = %icon%
label-empty-foreground = ${colors.white}
label-empty-background = ${colors.background}

[module/windowtitle]
type = internal/xwindow
format = <label>
label = %title%
format-prefix = " ﱮ "
format-foreground = ${colors.white}
format-background = ${colors.shade9}
label-maxlen = 32
format-prefix-foreground = ${colors.yellow}

[module/alsa]
type = internal/alsa
master-soundcard = hw:1
speaker-soundcard = hw:1
master-mixer = PCM
mapped = true
interval = 5
format-volume = "<label-volume> "
format-volume-padding = 0
format-volume-prefix = "墳 "
format-volume-prefix-foreground = ${colors.white}
format-volume-foreground = ${colors.white}
format-volume-background = ${colors.shade2}

[module/pulseaudio]
type = internal/pulseaudio
sink = alsa_output.usb-FiiO_DigiHug_USB_Audio-01.analog-stereo
use-ui-max = true
interval = 5
format-volume = <label-volume>
format-volume-prefix = "墳 "
format-volume-prefix-foreground = ${colors.blue}
format-volume-foreground = ${colors.blue}

[module/date]
type = internal/date
interval = 1.0
format = <label>
label = %date% %time%
format-prefix= " "
date = "%b %d %a"
time = "%H:%M "
date-alt = %B %d %A %Y
time-alt = %H:%M:%S
format-foreground = ${colors.white}
format-background = ${colors.shade3}
format-prefix-foreground = ${colors.white}

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = "  "
label-close = " 窱 cancel "
label-separator = " | "

format-background = ${colors.shade8}
format-foreground = ${colors.shade5}

menu-background = ${colors.shade8}
menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[module/filesystem]
type = internal/fs

; Mountpoints to display
mount-0 = /mnt/oppai

; Seconds to sleep between updates
; Default: 30
interval = 120

; Display fixed precision values
; Default: false
fixed-values = true

; Spacing between entries
; Default: 2
spacing = 2

; Available tags:
;   <label-mounted> (default)
;   <bar-free>
;   <bar-used>
;   <ramp-capacity>
format-mounted = <label-mounted>

; Available tags:
;   <label-unmounted> (default)
format-unmounted = <label-unmounted>

; Available tokens:
;   %mountpoint%
;   %type%
;   %fsname%
;   %percentage_free%
;   %percentage_used%
;   %total%
;   %free%
;   %used%
; Default: %mountpoint% %percentage_free%%
format-mounted-prefix = "  "
format-mounted-prefix-foreground = ${colors.yellow}
label-mounted = %mountpoint%: %used% used of %total%

; Available tokens:
;   %mountpoint%
; Default: %mountpoint% is not mounted
label-unmounted = %mountpoint%: not mounted
label-unmounted-foreground = #55

[module/memory]
type = internal/memory

; Seconds to sleep between updates
; Default: 1
interval = 3

; Available tags:
;   <label> (default)
;   <bar-used>
;   <bar-free>
;   <ramp-used>
;   <ramp-free>
;   <bar-swap-used>
;   <bar-swap-free>
;   <ramp-swap-used>
;   <ramp-swap-free>
format = <label>

; Available tokens:
;   %percentage_used% (default)
;   %percentage_free%
;   %gb_used%
;   %gb_free%
;   %gb_total%
;   %mb_used%
;   %mb_free%
;   %mb_total%
;   %percentage_swap_used%
;   %percentage_swap_free%
;   %mb_swap_total%
;   %mb_swap_free%
;   %mb_swap_used%
;   %gb_swap_total%
;   %gb_swap_free%
;   %gb_swap_used%

label = %gb_used% 易 %gb_free%
format-prefix = " "
format-prefix-foreground = ${colors.magenta}

[module/hostname]
type = custom/script
interval = 1200
format = "<label> "
;format-prefix = " "
format-prefix = " "
format-prefix-foreground = ${colors.white}
format-padding = 0
format-foreground = ${colors.white}
format-background = ${colors.shade4}
exec = echo "$(uname -nr)"
