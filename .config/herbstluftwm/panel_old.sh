#!/usr/bin/bash

# Main monitor
monitor=${1:-0}

# Padding
herbstclient pad $monitor 22 0 0 0

# Settings
RES="x22"
FONT="lemon:Regular:size=10"
FONT2="Siji:Regular:size=9"
FONT3="Kochi\Gothic:antialias=false:size=8"

# Define colors
BG="#202c33"
FG="#dcd9c5"
RED="#b43e46"
YLW="#f3b133"
BLU="#41878a"
MAG="#a43e64"
CYA="#578b76"

# Define icons
sm="%{F$RED} %{F-}"
sv="%{F$BLU} %{F-}"
sd="%{F$MAG} %{F-}"
st="%{F$CYA} %{F-}"

# functions
set -f

# Set a helper
uniq_linebuffered() {
    mawk -W interactive '$0 != l { print ; l=$0 ; fflush(); }' "$@"
}

# events
{   
    # Now playing
    mpc idleloop player | cat &
    mus_pid=$!

    # Volume
    while true ; do
        echo "vol $(pamixer --get-volume)%"
	sleep 1 || break
    done > >(uniq_linebuffered) &
    vol_pid=$!
    
    # Date
    while true ; do
        date +'date_min %b %d %a %H:%M'
        sleep 1 || break
    done > >(uniq_linebuffered) &
    date_pid=$!

    # Wait for events
    herbstclient --idle

    # Exiting; kill stray event-emitting processes
    kill $mus_pid $vol_pid $date_pid    

} 2> /dev/null | {
    TAGS=( $(herbstclient tag_status $monitor) )
    unset TAGS[${#TAGS[@]}]
    time=""
    song="nothing to see here"
    windowtitle="what have you done?"
    visible=true

        while true ; do
        echo -n "%{l}"
        for i in "${TAGS[@]}" ; do
            case ${i:0:1} in
                '#') # current tag
                    echo -n "%{B$RED}"
                    ;;
                '+') # active on other monitor
                    echo -n "%{B$YLW}"
                    ;;
                ':')
                    echo -n "%{B-}%{F-}"
                    ;;
                '!') # urgent tag
                    echo -n "%{B$YLW}"
                    ;;
                *)
                    echo -n "%{B-}%{F-}"
                    ;;
            esac
            echo -n " ${i:1} %{B-}%{F-}"
        done
	
	# center window title
	echo -n "%{c}$st${windowtitle//^/^^}"%{F-}
	
        # align right
        echo -n "%{r}"
        echo -n "$sm"
        echo -n "%{F$YLW}$song %{F-}"
        echo -n "$sv"
        echo -n "$volume "
        echo -n "$sd"
        echo -n "$date "
        echo ""
	
        # wait for next event
        read line || break
        cmd=( $line ) 
        # find out event origin
        case "${cmd[0]}" in
            tag*)
                TAGS=( $(herbstclient tag_status $monitor) )
                unset TAGS[${#TAGS[@]}]
                ;;
            mpd_player|player)
                song="$(mpc -f %title% current)"
                ;;
            vol)
                volume="${cmd[@]:1}"
                ;;
            date_min)
                date="${cmd[@]:1}"
                ;;
	    focus_changed|window_title_changed)
                windowtitle="${cmd[@]:2}"
                ;;
            quit_panel)
                exit
                ;;
            reload)
                exit
                ;;
        esac
    done
} 2> /dev/null | lemonbar -u 6 -g ${RES} -B ${BG}\
			  -F ${FG} -f ${FONT} -f ${FONT2} \
			  -f ${FONT3} & $1
wait
