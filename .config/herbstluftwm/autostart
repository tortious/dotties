#!/usr/bin/env bash

hc() {
    herbstclient "$@"
}

hc emit_hook reload

# remove all existing keybindings
hc keyunbind --all

# keybindings
Mod=Mod1    # Use alt as the main modifier

# tags
tag_names=("I" "II" "III" "IV" "V" "VI")
tag_keys=( {F1,F2,F3,F4,F5,F6} 0 )

hc rename default "${tag_names[0]}" || true
for i in ${!tag_names[@]} ; do
    hc add "${tag_names[$i]}"
    key="${tag_keys[$i]}"
    if ! [ -z "$key" ] ; then
        hc keybind "Control-$key" use_index "$i"
        hc keybind "$Mod-Shift-$key" move_index "$i"
    fi
done

# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
hc keybind $Mod-space                                                           \
            or , and . compare tags.focus.curframe_wcount = 2                   \
                     . cycle_layout +1 vertical horizontal max vertical grid    \
               , cycle_layout +1

# mouse
hc mouseunbind --all
hc mousebind $Mod-Button1 move
hc mousebind $Mod-Button2 zoom
hc mousebind $Mod-Button3 resize

# theme
hc attr theme.tiling.reset 1
hc attr theme.floating.reset 1
hc set frame_border_active_color '#222e35'
hc set frame_border_normal_color '#222e35'
hc set frame_bg_normal_color '#222e35'
hc set frame_bg_active_color '#222e35'
hc set frame_border_width 0
hc set always_show_frame 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 0
hc set frame_gap 0

hc attr theme.active.color '#222e35'
hc attr theme.normal.color '#222e35'
hc attr theme.urgent.color '#008080'
hc attr theme.inner_width 2
hc attr theme.inner_color '#3a4560'
hc attr theme.border_width 6
hc attr theme.floating.border_width 0
hc attr theme.floating.outer_width 0
hc attr theme.floating.outer_color '#3a4560'
hc attr theme.active.inner_color '#ac3a3a' 
hc attr theme.active.outer_color '#b43e46'
hc attr theme.background_color '#222e35'

hc set window_gap 0
hc set frame_padding 0
hc set smart_window_surroundings 0
hc set smart_frame_surroundings 1
hc set mouse_recenter_gap 0

# layouts
hc load I '(split horizontal:0.707800:0 
  (clients vertical:0 0x1200009) 
  (clients vertical:0 0x2e00003))'

hc load II '(split horizontal:0.500000:1 
  (clients vertical:0 0x1c0000a) 
  (clients vertical:0 0x2c00009))'

hc load III '(split horizontal:0.500000:1 
  (clients vertical:0 0x1c0000a) 
  (clients vertical:0 0x2c00009))'

hc load IV '(split horizontal:0.500000:1 
  (clients vertical:0 0x1c0000a) 
  (clients vertical:0 0x2c00009))'

# rules
hc unrule -F
#hc rule class=XTerm tag=3 # move all xterms to tag 3
hc rule focus=on # normally focus new clients
#hc rule focus=off # normally do not focus new clients
# give focus to most common terminals
#hc rule class~'(.*[Rr]xvt.*|.*[Tt]erm|Konsole)' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' pseudotile=on
hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off

# rules

# unlock, just to be sure
hc unlock

herbstclient set tree_style '╾│ ├└╼─┐'

# do multi monitor setup here, e.g.:
hc detect_monitors

# panel
killall lemonbar
#killall polybar
#hc pad 0 0 0 28
#hc pad 0 28 0 0
#hc pad 0 28 0 28
#hc pad 0 20 0 0
#polybar top &
#polybar bottom &
panel_old.sh

