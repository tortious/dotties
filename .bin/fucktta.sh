#!/bin/bash

for D in ./*; do
    if [ -d "$D" ]; then
        cd "$D"
        split2flac *.flac
        cd ..
    fi
done
