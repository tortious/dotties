#!/usr/bin/bash

# This panel requires a XFT patched lemonbar.
# I also use Siji font to get icon glyphs.
# Kochi Substitute gives a pretty good non
# antialiased Japanese characters out of the
# box. Lemon is from phallus and it's extremely
# small but it looks nice on panels.

# TODO; make skroll/scroller/zscroll to work on
# this script, especially with `mpc current`.
# I have no idea how to make it to work at the
# moment. $command | skroll -r doesn't work.

# Define settings
RES="310x28+1610+0" # WxH+X(offset)+Y(offset)
FNT="lemon:Regular:size=10" # main font
FNT2="Siji:Regular:size=9"  # glyph font
FNT3="Kochi\Gothic:antialias=false:size=9" # japanese font

# Define colors
BG="#202c33"  # background
FG="#dcd9c5"  # foreground
RED="#b43e46" # Red
YLW="#f3b133" # Yellow
BLU="#41878a" # Blue
MAG="#a43e64" # Magenta
CYA="#578b76" # Cyan

# Define icons and color them as well.
sm="%{F$RED} %{F-}" # music
sv="%{F$BLU} %{F-}" # volume
sd="%{F$MAG} %{F-}" # date

# Define how mpc should print information. 'head -c 17'
# is being used so the output won't go off the map.
# I print 'a lot' of information to keep things tied up.
cur=$(mpc -f '%title% by %artist% from %album%' current | head -c 17)

# Set functions
set -f

# Set an event helper
uniq_linebuffered() {
    mawk -W interactive '$0 != l { print ; l=$0 ; fflush(); }' "$@"
}

# Events
{   
    # Now playing
    mpc idleloop player | cat &
    mus_pid=$!
    
    # Volume
    while true ; do
        echo "vol $(pamixer --get-volume)%"
	sleep 5 || break
    done > >(uniq_linebuffered) &
    vol_pid=$!
    
    # Date
    while true ; do
        date +'date_min %b %d %a %H:%M'
        sleep 5 || break
    done > >(uniq_linebuffered) &
    date_pid=$!

} 2> /dev/null | {

        # Kill processes
        kill $mus_pid       # PID for mpd
        kill $vol_pid       # PID for volume
        kill $date_pid      # PID for date
        
        # Print stuff
    while true ; do
	echo -n "%{r}"      # align to the right
	echo -n "$sm"       # gluph for music
	echo -n "$song "    # now playing
	echo -n "$sv"       # glyph for volume
	echo -n "$volume "  # volume
	echo -n "$sd"       # glyph for date
	echo -n "$date "    # date
	echo ""             # needed to work

        # Wait for next event
        read line || break
        cmd=( $line ) 
        
        # Find out event origin
        case "${cmd[0]}" in
            mpd_player|player)
                song="$cur.." # dots are for style
                ;;
            vol)
                volume="${cmd[@]:1}"
                ;;            
            date_min)
                date="${cmd[@]:1}"
                ;;
        esac
    done
} 2> /dev/null | lemonbar -b -p -g ${RES} -B ${BG} -F \
                          ${FG} -f ${FNT} -f ${FNT2} -f \
                          ${FNT3} & $1
