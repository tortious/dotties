#!/usr/bin/bash

# Settings
RES="126x28+1786+10"
FONT="lemon:Regular:size=10"
FONT2="Siji:Regular:size=9"

# Define colors
BG="#202c33"
FG="#dcd9c5"
RED="#b43e46"

# Define icons
sd="%{F$RED} %{F-}"

# Set a helper
uniq_linebuffered() {
    mawk -W interactive '$0 != l { print ; l=$0 ; fflush(); }' "$@"
}

# events
{   
    while true ; do
        date +'date_min %b %d %a %H:%M'
        sleep 1 || break
    done > >(uniq_linebuffered) &
    date_pid=$!

} 2> /dev/null | {

        # kill processes
        kill $date_pid
        
        # nice boat
        while true ; do
        echo -n %{r}
        echo -n "$sd"
        echo -n "$date "
	echo ""

        # wait for next event
        read line || break
        cmd=( $line ) 
        
        # find out event origin
        case "${cmd[0]}" in
            date_min)
                date="${cmd[@]:1}"
                ;;
        esac
    done
} 2> /dev/null | lemonbar -b -p -g ${RES} -B ${BG} -F ${FG} -f ${FONT} -f ${FONT2} & $1
